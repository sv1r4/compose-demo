﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace compose_demo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            var config = new ConfigurationBuilder()
                .AddJsonFile("appsettings.json", optional:true)
                .AddEnvironmentVariables()
                .Build();

            var services = new ServiceCollection();

            services
                .Configure<InfluxConfig>(config.GetSection("InfluxConfig"))
                .AddSingleton<IInfluxClient, InfluxClient>()
                ;

            var sp = services.BuildServiceProvider();

            var c = sp.GetRequiredService<IInfluxClient>();

            c.Run();

        }
    }
}
