﻿using System;
using Microsoft.Extensions.Options;

namespace compose_demo
{
    public class InfluxClient : IInfluxClient
    {
        private readonly IOptions<InfluxConfig> _options;

        public InfluxClient(IOptions<InfluxConfig> options)
        {
            _options = options;
        }

        public void Run()
        {
            Console.WriteLine(_options.Value.Bucket);
        }
    }
}
